import { combineReducers, createStore } from "redux";
import { errorReducer } from "./errors/reducers";

const rootReducer = combineReducers({
	errors: errorReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

const store = createStore(rootReducer);

export default store;
