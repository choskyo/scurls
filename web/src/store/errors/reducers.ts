import { ADD_ERROR, ErrorAction, ErrorState, RMV_ERROR } from "./types";

const initialState: ErrorState = {
	errors: [] as { id: string; text: string }[],
};

export const errorReducer = (
	state = initialState,
	action: ErrorAction
): ErrorState => {
	switch (action.type) {
		case ADD_ERROR: {
			return {
				...state,
				errors: [
					...state.errors,
					{
						id: Math.random().toString(),
						text: action.error,
					},
				],
			};
		}
		case RMV_ERROR: {
			const errors = [...state.errors];
			const i = errors.findIndex((e) => e.id === action.errorId);
			errors.splice(i, 1);

			return {
				...state,
				errors,
			};
		}
		default: {
			return state;
		}
	}
};
