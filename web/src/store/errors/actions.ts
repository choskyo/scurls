import { ADD_ERROR, ErrorAction, RMV_ERROR } from "./types";

export const addError = (error: string): ErrorAction => {
	return {
		type: ADD_ERROR,
		error,
	};
};

export const removeError = (errorId: string): ErrorAction => {
	return {
		type: RMV_ERROR,
		errorId,
	};
};
