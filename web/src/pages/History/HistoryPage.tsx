import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { apiRequest } from "../../api/apiRequest";
import Scurl from "../../models/scurl";

const HistoryPage = () => {
	const history = useHistory();
	const [scurls, setScurls] = useState([] as Scurl[]);
	const [skip, setSkip] = useState(0);

	useEffect(() => {
		// tslint:disable-next-line: no-floating-promises
		(async () => {
			const res = await apiRequest(
				"scurls",
				`?skip=${skip}`,
				"GET",
				undefined,
				undefined,
				true
			);

			if (!res.ok) {
				return;
			}

			const scurlsHistory = (await res.json()).history;
			setScurls([...scurlsHistory]);
		})();
	}, [skip]);

	return (
		<div className="max-w-4xl mx-auto p-8">
			<span className="text-xl">History</span>
			<div className="p-4 grid grid-cols-8 mb-4 mt-4 border-b-4 border-gray-400">
				<span>ID</span>
				<span>Shortcode</span>
				<span className="col-span-5">Original</span>
				<span>Used X Times</span>
			</div>
			<div className="flex flex-col">
				{scurls.map((scurl) => (
					<div
						key={scurl.id}
						onClick={() => history.push(`/stats/${scurl.id}`)}
						className="p-4 bg-gray-100 shadow-md grid grid-cols-8 mb-4 cursor-pointer">
						<span>{scurl.id}</span>
						<span>{scurl.shortCode}</span>
						<span className="col-span-5">
							{scurl.originalURL.length > 50
								? `${scurl.originalURL.slice(0, 50)}...`
								: scurl.originalURL}
						</span>
						<span>{scurl.usedXTimes}</span>
					</div>
				))}
			</div>
			<div className="grid grid-cols-2 gap-12 mr-12 ml-12">
				<button
					className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
					type="button"
					disabled={skip === 0}
					onClick={() => setSkip(skip - 15)}>
					Prev
				</button>
				<button
					className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
					type="button"
					onClick={() => setSkip(skip + 15)}>
					Next
				</button>
			</div>
		</div>
	);
};

export default HistoryPage;
