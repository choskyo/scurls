import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { apiRequest } from "../../api/apiRequest";
import Scurl from "../../models/scurl";

interface Props {
	scurl: Scurl;
}

const StatsPage = (props: Props) => {
	const history = useHistory();
	const id = history.location.pathname.split("/")[2];
	const [scurl, setScurl] = useState({} as Scurl);

	useEffect(() => {
		if (!props.scurl && !scurl.originalURL) {
			// tslint:disable-next-line: no-floating-promises
			(async () => {
				console.log(id);
				const res = await apiRequest(
					"scurls",
					id,
					"GET",
					undefined,
					undefined,
					true
				);

				if (!res.ok) {
					return;
				}

				setScurl(await res.json());
			})();
		} else if (!scurl.originalURL) {
			setScurl(props.scurl);
		}
	}, [scurl]);

	return (
		<div className="max-w-4xl mx-auto p-8">
			{scurl && scurl.originalURL && (
				<>
					<span className="text-xl">Stats for SCUrl: {scurl.id}</span>
					<div className="p-4 border-b-2 border-gray-200 grid grid-cols-4 my-2 max-w-xl mx-auto">
						<span>ID</span>
						<span className="col-span-3">{scurl.id}</span>
					</div>
					<div className="p-4 border-b-2 border-gray-200 grid grid-cols-4 my-2 max-w-xl mx-auto">
						<span>ShortCode</span>
						<span className="col-span-3">{scurl.shortCode}</span>
					</div>
					<div className="p-4 border-b-2 border-gray-200 grid grid-cols-4 my-2 max-w-xl mx-auto">
						<span>Original URL</span>
						<a
							className="underline col-span-3"
							href={scurl.originalURL}>
							{scurl.originalURL}
						</a>
					</div>
					<div className="p-4 border-b-2 border-gray-200 grid grid-cols-4 my-2 max-w-xl mx-auto">
						<span>Used X Times</span>
						<span className="col-span-3">{scurl.usedXTimes}</span>
					</div>
				</>
			)}
		</div>
	);
};

export default StatsPage;
