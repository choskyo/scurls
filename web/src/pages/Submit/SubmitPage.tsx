import React, { ChangeEvent, useState } from "react";
import { apiRequest } from "../../api/apiRequest";

const SubmitPage = () => {
	const [originalUrl, setOriginalUrl] = useState("");
	const [shortenedUrl, setShortenedUrl] = useState("");
	const [errors, setErrors] = useState([] as string[]);

	const getShortenedURL = async () => {
		const res = await apiRequest(
			"scurls",
			"",
			"POST",
			{
				url: originalUrl,
			},
			undefined,
			true
		);

		if (!res.ok) {
			setErrors([(await res.json()).err]);
			return;
		}

		setShortenedUrl(
			`http://localhost:3000/${(await res.json()).scurl.shortCode}`
		);
	};

	return (
		<div className="w-full mt-20 flex flex-col max-w-lg mx-auto">
			<span className="mx-auto mb-8 text-xl">Shorten URL</span>
			<input
				className="mb-8 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
				id="url"
				type="text"
				placeholder="URL"
				value={originalUrl}
				onChange={(e: ChangeEvent<HTMLInputElement>) =>
					setOriginalUrl(e.target.value)
				}
			/>
			{shortenedUrl && (
				<>
					<label className="block text-gray-700 text-sm font-bold mb-2">
						Shortened
					</label>
					<div className="mb-8 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
						{shortenedUrl}
					</div>
				</>
			)}
			{errors.length > 0 && (
				<div className="bg-red-200 text-red-900 rounded-lg p-8 mb-8 border-red-900 border-2">
					<ul className="list-disc">
						{errors.map((err) => (
							<li key={err}>{err}</li>
						))}
					</ul>
				</div>
			)}
			<button
				className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
				type="button"
				onClick={() => getShortenedURL()}>
				Shorten
			</button>
		</div>
	);
};

export default SubmitPage;
