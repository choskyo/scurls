import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { apiRequest } from "../../api/apiRequest";

const VisitPage = () => {
	const history = useHistory();
	const shortCode = history.location.pathname.split("/")[1];
	const dispatch = useDispatch();

	useEffect(() => {
		// tslint:disable-next-line: no-floating-promises
		(async () => {
			const res = await apiRequest(
				"scurls",
				`v/${shortCode}`,
				"GET",
				undefined,
				undefined,
				true
			);

			if (!res.ok) {
				return;
			}

			const scurl = await res.json();

			window.location = scurl.originalURL;
		})();
	});

	return <></>;
};

export default VisitPage;
