import React, { ChangeEvent, useState } from "react";
import { useHistory } from "react-router-dom";
import { apiRequest } from "../../api/apiRequest";

const SplashPage = () => {
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [registerSuccess, setRegisterSuccess] = useState(false);
	const history = useHistory();

	const register = async () => {
		const res = await apiRequest(
			"auth",
			"register",
			"POST",
			{
				username,
				password,
			},
			undefined,
			false
		);

		setRegisterSuccess(res.ok);
	};

	const login = async () => {
		const res = await apiRequest(
			"auth",
			"login",
			"POST",
			{
				username,
				password,
			},
			undefined,
			false
		);

		if (res.ok) {
			localStorage.setItem("token", (await res.json()).token);

			history.push("/submit");
		}
	};

	return (
		<div className="w-full mt-20 flex flex-col justify-center items-center">
			<div className="p-12 bg-gray-200 rounded-lg shadow-lg">
				<div className="mb-4">
					<label
						className="block text-gray-700 text-sm font-bold mb-2"
						htmlFor="username">
						Username
					</label>
					<input
						className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
						id="username"
						type="text"
						placeholder="Username"
						value={username}
						onChange={(e: ChangeEvent<HTMLInputElement>) =>
							setUsername(e.target.value)
						}
					/>
				</div>
				<div className="mb-4">
					<label
						className="block text-gray-700 text-sm font-bold mb-2"
						htmlFor="password">
						Password
					</label>
					<input
						className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
						id="password"
						type="password"
						placeholder="********"
						value={password}
						onChange={(e: ChangeEvent<HTMLInputElement>) =>
							setPassword(e.target.value)
						}
					/>
				</div>
				{registerSuccess && (
					<div className="bg-green-200 text-green-900 rounded-lg p-8 mb-8 border-green-900 border-2">
						<ul className="list-disc">
							Registration successful, you can now sign in.
						</ul>
					</div>
				)}
				<div className="grid grid-cols-2 gap-4">
					<button
						className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
						type="button"
						onClick={() => login()}>
						Sign In
					</button>
					<button
						className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
						type="button"
						onClick={() => register()}>
						Register
					</button>
				</div>
			</div>
		</div>
	);
};

export default SplashPage;
