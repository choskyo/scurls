import { createBrowserHistory } from "history";
import { Router } from "react-router-dom";

export const history = createBrowserHistory();

export default class CustomRouter extends Router {
	history: any;
}
