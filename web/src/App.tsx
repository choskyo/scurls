import React from "react";
import {
	Route,
	RouteComponentProps,
	Switch,
	withRouter,
} from "react-router-dom";
import "./App.css";
import ErrorContainer from "./components/ErrorContainer";
import Header from "./components/Header";
import HistoryPage from "./pages/History/HistoryPage";
import SplashPage from "./pages/SplashPage/SplashPage";
import StatsPage from "./pages/Stats/StatsPage";
import SubmitPage from "./pages/Submit/SubmitPage";
import VisitPage from "./pages/Visit/VisitPage";

const App = withRouter((props: RouteComponentProps) => (
	<div className="min-h-full">
		<ErrorContainer />
		<Header location={props.location.pathname} />
		<Switch>
			<Route exact path="/" component={SplashPage} />
			<Route exact path="/submit" component={SubmitPage} />
			<Route exact path="/history" component={HistoryPage} />
			<Route exact path="/stats/:id" component={StatsPage} />
			<Route exact path="/:shortCode" component={VisitPage} />
		</Switch>
	</div>
));

export default App;
