import React from "react";
import { useSelector } from "react-redux";
import { AppState } from "../store/store";
import ErrorToast from "./ErrorToast";

const ErrorContainer = () => {
	const errors = useSelector((state: AppState) => state.errors.errors);

	return (
		<div
			className="absolute"
			style={{
				top: 100,
				left: 30,
			}}>
			{errors.map((err) => (
				<ErrorToast key={err.id} error={err} />
			))}
		</div>
	);
};

export default ErrorContainer;
