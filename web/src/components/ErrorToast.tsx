import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { removeError } from "../store/errors/actions";

interface Props {
	error: { id: string; text: string };
}

const ErrorToast = (props: Props) => {
	const dispatch = useDispatch();

	useEffect(() => {
		setTimeout(() => {
			dispatch(removeError(props.error.id));
		}, 5000);
	}, []);

	return (
		<div className="bg-gray-100 rounded-lg shadow-lg w-full max-w-sm mb-8">
			<div className="bg-red-500 rounded-t-lg text-white p-2">Error</div>
			<div className="px-2 py-4">{props.error.text}</div>
		</div>
	);
};

export default ErrorToast;
