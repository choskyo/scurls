import React, { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";

interface Props {
	location: string;
}

const Header = (props: Props) => {
	const history = useHistory();
	const [showNav, setShowNav] = useState(false);

	useEffect(() => {
		setShowNav(props.location !== "/");
	}, [props.location]);

	return (
		<nav className="flex items-center justify-between flex-wrap bg-blue-800 p-6 text-white">
			<div className="flex items-center flex-shrink-0 mr-6">SCUrls</div>
			<div className={showNav ? "block" : "hidden"}>
				<Link
					to="/submit"
					className="hover:text-blue-200 hover:underline mr-10">
					Submit
				</Link>
				<Link
					to="/history"
					className="hover:text-blue-200 hover:underline mr-10">
					History
				</Link>
				<button
					className="appearance-none hover:text-blue-200 hover:underline"
					onClick={() => {
						localStorage.clear();
						history.push("/");
					}}>
					Log out
				</button>
			</div>
		</nav>
	);
};

export default Header;
