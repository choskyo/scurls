interface Scurl {
	id: number;
	shortCode: string;
	originalURL: string;
	usedXTimes: number;
}

export default Scurl;
