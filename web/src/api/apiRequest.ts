import { history } from "../CustomRouter";
import { addError } from "../store/errors/actions";
import store from "../store/store";

export const apiRoot = "https://localhost:5001";

export const apiRequest = async (
	service: "auth" | "scurls",
	endpoint: string,
	method: "GET" | "POST" | "PUT",
	body: any = undefined,
	extraHeaders: Map<string, string> = new Map(),
	authenticated: boolean = true
) => {
	try {
		const init: RequestInit = {
			headers: {},
			method,
		};
		const headers: any = {};

		extraHeaders.forEach((v, k) => {
			headers[k] = v;
		});

		if (authenticated) {
			headers.Authorization = `Bearer ${
				localStorage.getItem("token") || ""
			}`;
		}

		if (method !== "GET" && !headers["Content-Type"]) {
			headers["Content-Type"] = "application/json";

			if (body) {
				init.body = JSON.stringify(body);
			}
		}

		init.headers = headers;

		const result = await fetch(`${apiRoot}/${service}/${endpoint}`, init);

		if (!result.ok) {
			const resBody = (await result.json()) as { err: string };

			if (resBody.err) {
				store.dispatch(addError(resBody.err));
			} else {
				store.dispatch(addError(result.statusText));
			}

			if (result.status === 401) {
				history.push("/");
			}
		}

		return result;
	} catch (err) {
		store.dispatch(addError(err.message));

		return {
			ok: false,
		} as Response;
	}
};
