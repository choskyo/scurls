using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Scurls.Core.Services;
using Xunit;

namespace Scurls.Core.Tests.Services
{
    public class AuthServiceTests
    {
        private DbContextOptions<ScurlsContext> contextOptions
            => new DbContextOptionsBuilder<ScurlsContext>().UseSqlite(CreateInMemoryDatabase()).Options;

        private static SqliteConnection CreateInMemoryDatabase()
        {
            var connection = new SqliteConnection("Filename=:memory:");
            connection.Open();
            return connection;
        }

        private (IAuthService, IUserService) GetServices()
        {
            var context = new ScurlsContext(contextOptions);
            context.Database.Migrate();

            return (new AuthService(context), new UserService(context));
        }

        [Fact]
        public void Login_FailsWithWrongPassword()
        {
            var (auth, user) = GetServices();
            user.Add("test1", "test2");

            var token = auth.Login("test1", "test1");

            Assert.Equal("", token);
        }

        [Fact]
        public void Login_ReturnsTokenWithCorrectPassword()
        {
            var (auth, user) = GetServices();
            user.Add("test1", "test2");

            var token = auth.Login("test1", "test2");

            Assert.NotEqual("", token);
        }
    }
}