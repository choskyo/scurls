using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Scurls.Core.Services;
using Xunit;

namespace Scurls.Core.Tests.Services
{
    public class UserServiceTests
    {
        private DbContextOptions<ScurlsContext> contextOptions
            => new DbContextOptionsBuilder<ScurlsContext>().UseSqlite(CreateInMemoryDatabase()).Options;

        private static SqliteConnection CreateInMemoryDatabase()
        {
            var connection = new SqliteConnection("Filename=:memory:");
            connection.Open();
            return connection;
        }

        private IUserService GetService()
        {
            var context = new ScurlsContext(contextOptions);
            context.Database.Migrate();

            return new UserService(context);
        }

        [Fact]
        public void Add_InsertsNewUser()
        {
            var (success, error) = GetService().Add("test1", "test1");

            Assert.True(success);
            Assert.Equal("", error);
        }

        public void Add_FailsCorrectly()
        {
            var service = GetService();

            var (success, error) = service.Add("test5", "test5");

            (success, error) = service.Add("test5", "test5");
            Assert.False(success);
            Assert.Equal("Username already taken.", error);

            (success, error) = service.Add("", "test5");
            Assert.False(success);
            Assert.Equal("Username or password missing but required.", error);

            (success, error) = service.Add("test5", "");
            Assert.False(success);
            Assert.Equal("Username or password missing but required.", error);
        }


        [Fact]
        public void FindOneByUsername_GetsCorrectUser()
        {
            var service = GetService();
            service.Add("test1", "test1");

            var user = service.FindOneByUsername("test1");

            Assert.Equal("test1", user.Username);

        }

        [Fact]
        public void FindOneByUsername_NullIfNoUser()
        {
            var service = GetService();

            var user = service.FindOneByUsername("test6");

            Assert.Null(user);

        }
    }
}