using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Scurls.Core.Services;
using Xunit;

namespace Scurls.Core.Tests.Services
{
    public class ScurlServiceTests
    {
        private DbContextOptions<ScurlsContext> contextOptions
            => new DbContextOptionsBuilder<ScurlsContext>().UseSqlite(CreateInMemoryDatabase()).Options;

        private static SqliteConnection CreateInMemoryDatabase()
        {
            var connection = new SqliteConnection("Filename=:memory:");
            connection.Open();
            return connection;
        }

        private IScurlService GetService()
        {
            var context = new ScurlsContext(contextOptions);
            context.Database.Migrate();

            // Insert test user
            context.Users.Add(new Models.User()
            {
                Username = "test",
                Password = "test"
            });
            context.SaveChanges();

            return new ScurlService(context);
        }

        [Fact]
        public void Generate_InsertsShortcodeCorrectly()
        {
            var svc = GetService();
            var (scurl, error) = svc.Generate(1, "https://google.com");

            Assert.Equal("", error);
            Assert.Equal("b", scurl.ShortCode);
        }

        [Fact]
        public void Generate_Fails_WrongProtocol()
        {
            var svc = GetService();
            var (scurl, error) = svc.Generate(1, "ftp://anything");

            Assert.NotEqual("", error);
            Assert.Null(scurl);
        }

        [Fact]
        public void Generate_Fails_InvalidURL()
        {
            var svc = GetService();
            var (scurl, error) = svc.Generate(1, "https://googlecom");

            Assert.NotEqual("", error);
            Assert.Null(scurl);
        }
    }
}