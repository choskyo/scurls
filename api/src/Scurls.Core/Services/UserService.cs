using System.Collections.Generic;
using System.Linq;
using Scurls.Core.Models;

namespace Scurls.Core.Services
{
    public class UserService : IUserService
    {
        private ScurlsContext DB { get; }

        public UserService(ScurlsContext db)
        {
            DB = db;
        }

        public (bool success, string error) Add(string username, string password)
        {
            if (username == "" || password == "")
            {
                return (false, "Username or password missing but required.");
            }

            if (DB.Users.Any(u => u.Username == username))
            {
                return (false, "Username already taken.");
            }

            DB.Users.Add(new User
            {
                Username = username,
                Password = BCrypt.Net.BCrypt.HashPassword(password)
            });

            DB.SaveChanges();

            return (true, "");
        }

        public User FindOneByUsername(string username)
        {
            return DB.Users.FirstOrDefault(u => u.Username == username);
        }
    }
}