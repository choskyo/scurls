using System.Collections.Generic;
using Scurls.Core.Models;

namespace Scurls.Core.Services
{
    public interface IScurlService
    {
        bool RegisterVisit(int id);
        Scurl GetIndividualByID(int id);
        Scurl GetIndividualBySC(string shortCode);
        IEnumerable<Scurl> GetHistory(int userID, int skip, int take);
        (Scurl, string) Generate(int userID, string originalURL);
    }
}