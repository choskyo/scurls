using Scurls.Core.Models;

namespace Scurls.Core.Services
{
    public interface IAuthService
    {
        string Login(string username, string password);
    }
}