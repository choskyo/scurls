using Scurls.Core.Models;

namespace Scurls.Core.Services
{
    public interface IUserService
    {
        User FindOneByUsername(string username);
        (bool success, string error) Add(string username, string password);
    }
}