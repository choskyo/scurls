using System;
using System.Collections.Generic;
using System.Linq;
using Scurls.Core.Models;

namespace Scurls.Core.Services
{
    public class ScurlService : IScurlService
    {
        private ScurlsContext DB { get; }

        public ScurlService(ScurlsContext db)
        {
            DB = db;
        }

        public bool RegisterVisit(int id)
        {
            var scurl = DB.Scurls.FirstOrDefault(s => s.ID == id);

            if (scurl == null)
            {
                return false;
            }

            scurl.UsedXTimes++;

            DB.Scurls.Update(scurl);
            DB.SaveChanges();

            return true;
        }

        public Scurl GetIndividualByID(int id)
        {
            return DB.Scurls.FirstOrDefault(s => s.ID == id);
        }

        public Scurl GetIndividualBySC(string shortCode)
        {
            return DB.Scurls.FirstOrDefault(s => s.ShortCode == shortCode);
        }

        public IEnumerable<Scurl> GetHistory(int userID, int skip, int take)
        {
            return DB.Scurls.Where(s => s.UserID == userID)
                            .OrderByDescending(s => s.ID)
                            .Skip(skip)
                            .Take(take);
        }

        public (Scurl, string) Generate(int userID, string originalURL)
        {
            Uri uriResult;
            if (!Uri.TryCreate(originalURL, UriKind.Absolute, out uriResult)
                || (uriResult.Scheme != Uri.UriSchemeHttp && uriResult.Scheme != Uri.UriSchemeHttps)
                || !uriResult.IsAbsoluteUri)
            {
                return (null, "URL is not valid HTTP/HTTPS address.");
            }

            if (!Utils.Utils.AccessibleURI(uriResult).Result)
            {
                return (null, "URL could not be reached.");
            }

            var scurl = new Scurl()
            {
                UserID = userID,
                OriginalURL = originalURL
            };

            DB.Scurls.Add(scurl);
            DB.SaveChanges();

            scurl.ShortCode = Utils.Utils.GetShortCode(scurl.ID);

            DB.Update(scurl);
            DB.SaveChanges();

            return (scurl, "");
        }
    }
}