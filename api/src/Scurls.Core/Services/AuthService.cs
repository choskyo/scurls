using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Scurls.Core.Models;

namespace Scurls.Core.Services
{
    public class AuthService : IAuthService
    {
        private ScurlsContext DB { get; }
        private SigningCredentials JWTSigningCreds { get; }

        public AuthService(ScurlsContext db)
        {
            DB = db;
            JWTSigningCreds = new SigningCredentials(
                new SymmetricSecurityKey(Encoding.UTF8.GetBytes("ABCDEFGHIJKLMNOPQRSTUVWXYZ")),
                SecurityAlgorithms.HmacSha256
            );
        }

        public string Login(string username, string password)
        {
            var user = DB.Users.FirstOrDefault(u => u.Username == username);

            if (user == null || !BCrypt.Net.BCrypt.Verify(password, user.Password)) return "";

            var token = new JwtSecurityToken(
                issuer: "localhost:5001",
                audience: "localhost:3000",
                expires: DateTime.Now.AddHours(1),
                signingCredentials: JWTSigningCreds,
                claims: new[] {
                    new Claim("id", user.ID.ToString()),
                }
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}