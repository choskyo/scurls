using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Scurls.Core.Utils
{
    public class Utils
    {
        private static string alphaNum = "abcdefghijklmnopqrstuvwxyz0123456789";

        public static string GetShortCode(int scurlID)
        {
            var num = scurlID;
            var sc = "";

            while (num > 0)
            {
                sc += alphaNum[num % 36];
                num /= 36;
            }

            return sc;
        }

        public static async Task<bool> AccessibleURI(Uri url)
        {
            using var client = new HttpClient();

            try
            {
                var result = await client.GetAsync(url);

                return result.IsSuccessStatusCode;
            }
            catch
            {
                return false;
            }
        }
    }
}