using System.Collections.Generic;

namespace Scurls.Core.Models
{
    public class User
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public List<Scurl> Scurls { get; } = new List<Scurl>();
    }
}