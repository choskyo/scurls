namespace Scurls.Core.Models
{
    public class Scurl
    {
        public int ID { get; set; }
        public string ShortCode { get; set; }
        public string OriginalURL { get; set; }
        public int UsedXTimes { get; set; }

        public int UserID { get; set; }
        public User User { get; set; }
    }
}