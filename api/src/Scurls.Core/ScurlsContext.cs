using Microsoft.EntityFrameworkCore;
using Scurls.Core.Models;

namespace Scurls.Core
{
    public class ScurlsContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Scurl> Scurls { get; set; }

        public ScurlsContext(DbContextOptions<ScurlsContext> options)
            : base(options) { }
    }
}