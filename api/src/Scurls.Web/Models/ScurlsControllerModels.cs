using System.Text.Json.Serialization;

namespace Scurls.Web.Models
{
    public class ScurlsPublicVM
    {
        [JsonPropertyName("id")]
        public int ID { get; set; }

        [JsonPropertyName("shortCode")]
        public string shortCode { get; set; }

        [JsonPropertyName("originalURL")]
        public string originalURL { get; set; }
    }

    public class ScurlsDetailedVM : ScurlsPublicVM
    {
        [JsonPropertyName("usedXTimes")]
        public int UsedXTimes { get; set; }

        [JsonPropertyName("userID")]
        public int UserID { get; set; }
    }

    public class ScurlsGenerateRequest
    {
        [JsonPropertyName("url")]
        public string URL { get; set; }
    }
}