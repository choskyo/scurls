using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Scurls.Core;
using Scurls.Core.Services;
using Scurls.Web.Models;

namespace Scurls.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthController : ControllerBase
    {
        private IAuthService AuthService { get; }
        private IUserService UserService { get; }

        public AuthController(IAuthService authService, IUserService userService)
        {
            AuthService = authService;
            UserService = userService;
        }

        [HttpPost("register")]
        public ActionResult Register([FromBody] RegisterUserRequest m)
        {
            var (success, err) = UserService.Add(m.Username, m.Password);

            if (!success)
            {
                return StatusCode(400, new { err = err });
            }

            return StatusCode(200);
        }

        [HttpPost("login")]
        public ActionResult<LoginResponse> Login([FromBody] RegisterUserRequest m)
        {
            string token = AuthService.Login(m.Username, m.Password);

            if (token == "")
            {
                return StatusCode(400, new { err = "Incorrect login details." });
            }

            return StatusCode(200, new LoginResponse { Token = token });
        }
    }
}