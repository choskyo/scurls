using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Scurls.Core.Services;
using Scurls.Web.Extensions;
using Scurls.Web.Models;

namespace Scurls.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ScurlsController : ControllerBase
    {
        private IScurlService ScurlService { get; set; }

        public ScurlsController(IScurlService scurlService)
        {
            ScurlService = scurlService;
        }

        [AllowAnonymous]
        [HttpGet("v/{shortCode}")]
        public ActionResult Visit([FromRoute] string shortCode)
        {
            if (shortCode == "")
            {
                return StatusCode(400, new { err = "Missing shortcode." });
            }

            var scurl = ScurlService.GetIndividualBySC(shortCode);

            if (scurl == null)
            {
                return StatusCode(400, new { err = "Shortcode not in use." });
            }

            if (!ScurlService.RegisterVisit(scurl.ID))
            {
                // Log that this part failed without interrupting user.
            }

            return Redirect(scurl.OriginalURL);
        }

        [Authorize]
        [HttpGet("{id:int}")]
        public ActionResult<ScurlsDetailedVM> Details([FromRoute] int id)
        {
            var scurl = ScurlService.GetIndividualByID(id);

            if (scurl == null)
            {
                return StatusCode(400, new { err = "SCUrl not found with that ID." });
            }

            if (scurl.UserID != this.UserID())
            {
                return StatusCode(401, new { err = "Unauthorised to view this SCUrl." });
            }

            return StatusCode(200, new ScurlsDetailedVM
            {
                ID = scurl.ID,
                originalURL = scurl.OriginalURL,
                shortCode = scurl.ShortCode,
                UsedXTimes = scurl.UsedXTimes,
                UserID = scurl.UserID
            });
        }

        [Authorize]
        [HttpGet("")]
        public ActionResult<ScurlsDetailedVM[]> History([FromQuery] int skip = 0, [FromQuery] int take = 10)
        {
            var history = ScurlService.GetHistory(this.UserID(), skip, take);

            return StatusCode(200, new { history = history });
        }

        [Authorize]
        [HttpPost("")]
        public ActionResult Generate([FromBody] ScurlsGenerateRequest m)
        {
            var (scurl, err) = ScurlService.Generate(this.UserID(), m.URL);

            if (scurl == null)
            {
                return StatusCode(400, new { err = err });
            }

            return StatusCode(200, new { scurl = scurl });
        }
    }
}