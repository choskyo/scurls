using Microsoft.AspNetCore.Mvc;

namespace Scurls.Web.Extensions
{
    public static class ControllerBaseExtensions
    {
        public static int UserID(this ControllerBase c)
            => System.Convert.ToInt32(c.User.FindFirst("id").Value);
    }
}